package com.company;

import java.util.Random;

/**
 * Created by user on 06.07.2017.
 */
public class Sea {
    public static final int SIZE = 10;
    public static final Random RANDOM = new Random();
    public static final char SYMBOL = 2145;

    private int[][] field;

    public Sea() {
        field = new int[SIZE][SIZE];
    }

    public void fillArray() {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                field[i][j] = RANDOM.nextInt(2);
            }
        }
    }

    public void action(int x, int y) {
        try {
            if (x < 0 || x > field.length - 1 || y < 0 || y > field[0].length - 1) {
                throw new SeaBattleException("You enter incorrect coordinats");
            }

            if (field[x][y] == 0) {
                System.out.println("Miss!");
            } else {
                System.out.println("Kill!");
                field[x][y] = 0;
                System.out.println();
            }
        } catch (SeaBattleException e) {
            System.out.println(e.getMessage());
        }

    }

    public void printArray() {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                if (field[i][j] == 0) {
                    System.out.print("  ");
                } else {
                    System.out.print(SYMBOL);
                }
            }
            System.out.println();
        }
    }
}
