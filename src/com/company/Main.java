package com.company;

import java.util.Scanner;

public class Main {
    public static final Scanner CONSOLE_SCANER = new Scanner(System.in);


    public static void main(String[] args) {
        Sea sea = new Sea();
        sea.fillArray();
        sea.printArray();

        while (true) {

            printMenu();

            int choose = CONSOLE_SCANER.nextInt();

            if (choose == 1) {
                System.out.println("Enter X: ");
                int x = CONSOLE_SCANER.nextInt();

                System.out.println("Enter Y: ");
                int y = CONSOLE_SCANER.nextInt();

                sea.action(x, y);

                sea.printArray();
            } else if (choose != 1) {
                break;
            }

        }
    }

    private static void printMenu() {
        System.out.println("If you want to type coordinates, type 1" +
                "\nIf yo to exit, please enter any number");
    }

}