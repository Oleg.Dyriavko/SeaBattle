package com.company;

/**
 * Created by user on 06.07.2017.
 */
public class SeaBattleException extends Exception {
    public SeaBattleException(String message) {
        super(message);
    }
}
